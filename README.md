Bună bună prieteniii✨️🐊

Sunt sigură că acest mesaj vă găsește cu bine, și că sunteți pregătiți de cele ce vor urma. Ca să nu vă mai reținem vom trece direct la subiect😎

Primul pas ar fi👆 **clonarea proiectului** sau **descărcarea lui**, lucru care se poate realiza cu ajutorul acestui **[TUTORIAL](https://www.youtube.com/watch?v=OWaZXtgq28c)**.

Și acum fără alte adăugări aici se află minunata **✨️❗️[TEMĂ](https://www.youtube.com/watch?v=MGhVON7wFXs&feature=youtu.be)❗️✨**️.

Acum câteva materiale sau site-uri folositoare pentru a va ușura munca ar fi:
**✨️[Texte](https://docs.google.com/document/d/1AJM7C-NgOVjhNGoQVT_w_Ee9qW3jwlURsgV6YcZ_yyI/edit)** ->  textele care se găsesc pe site
**✨️[Font Awesome ](https://fontawesome.com/)**-> pentru iconițe
**✨️[Google Fonts ](https://fonts.google.com/)** -> fontul folosit e Poppins

Bineînțeles, site-ul n-ar fi complet fără **responsiveness📱**, ne-ar plăcea să vedem cum vă descurcați în realizarea acestuia.

**NU ESTE** obligatoriu însă, introducerea unei secțiuni noi pe site, dar am vrea să vă vedem imaginația în acțiune.🌈

Foarte important este să nu uitați că Google🌐 este la un pas distanță, iar tema va trimisă ca arhivă denumită **TemaIT_FrontEnd_NumePrenume**.

**Vă urăm mult succes!!!🐊💚**